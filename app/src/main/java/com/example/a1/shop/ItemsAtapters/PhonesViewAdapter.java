package com.example.a1.shop.ItemsAtapters;

import android.content.Context;
import android.widget.ArrayAdapter;

import com.example.a1.shop.R;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;


public class PhonesViewAdapter extends ArrayAdapter<String> {

    private static final List<String> phones = new ArrayList<String>();

    public PhonesViewAdapter(@NonNull Context context) {
        super(context, android.R.layout.simple_list_item_2, phones);
    }


}
