package com.example.a1.shop.ItemsAtapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;

import com.example.a1.shop.Objects.Kit;
import com.example.a1.shop.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class KitViewAdapter extends RecyclerView.Adapter<KitViewAdapter.ViewHolder> {
    Context context;
    private List<Kit> kits;
    private KitViewAdapter.Listener selectListener;
    private CardView cardView;

    public KitViewAdapter(Context context, List<Kit> kits) {
        this.context = context;
        this.kits = kits;
        this.cardView = cardView;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        ViewHolder(CardView cardView) {
            super(cardView);
            this.cardView = cardView;
        }
    }

    public KitViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        int res = R.layout.fragment_kit_card;
        CardView cv = (CardView)LayoutInflater.from(parent.getContext()).inflate(res, parent, false);

        ViewHolder vh = new ViewHolder(cv);
        return vh;
    }

    public void onBindViewHolder( ViewHolder holder, int position) {
        CardView cardView = holder.cardView;

        configureView(cardView, position, holder);
    }

    private void configureView(CardView cardView, int position, final ViewHolder holder) {
        Kit item = kits.get(position);

        TextView prise = cardView.findViewById(R.id.price_text_view);
        prise.setText(item.title);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectListener != null) {
                    selectListener.onClick(holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public long getItemId(int i) {
        return kits.size();
    }

    @Override
    public int getItemCount() {
        return kits.size();
    }

    public interface Listener {
        void onClick(int position);
    }
}
