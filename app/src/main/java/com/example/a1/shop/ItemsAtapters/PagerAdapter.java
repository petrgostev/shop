package com.example.a1.shop.ItemsAtapters;

import android.media.Image;
import android.os.Bundle;

import com.example.a1.shop.Fragments.PagerFragment;

import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentStatePagerAdapter;


public class PagerAdapter extends FragmentPagerAdapter {

    List<Image> images;

    public PagerAdapter(FragmentManager fm, List<Image> images) {
        super(fm);
        this.images = images;
    }

    @Override
    public Fragment getItem(int position) {
        return this.images.get(position);
    }

    @Override
    public int getCount() {
        return data.size();
    }
}
