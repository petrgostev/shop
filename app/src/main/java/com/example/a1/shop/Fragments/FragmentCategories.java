package com.example.a1.shop.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.example.a1.shop.DataService;
import com.example.a1.shop.ItemsAtapters.CategoryViewAdapter;
import com.example.a1.shop.Objects.Category;
import com.example.a1.shop.R;
import com.example.a1.shop.StartActivity;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


public class FragmentCategories extends Fragment {

    protected RecyclerView recyclerView;
    private ArrayList<Category> categories;
    private CategoryViewAdapter categoryViewAdapter;

    protected boolean isEdit = false;

    private boolean editOn = false;
    private boolean showDeleteButton = false;

    private MenuItem infoItem;
    private MenuItem cancelItem;
    private MenuItem basketItem;

    private View.OnClickListener menuListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        setMenuVisibility(true);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.fragment_categories_list, container, false);

        recyclerView = view.findViewById(R.id.recycler_category_view);

        show();

        String title = "Категории";

        if (getActivity() != null) {
            getActivity().setTitle(title);
        }

        return view;
    }

    public void show() {

        categories = DataService.getInstance().getCategoriesList();
        categoryViewAdapter = new CategoryViewAdapter(getContext(), categories);
        categoryViewAdapter.setSelectListener(new CategoryViewAdapter.Listener() {
            @Override
            public void onClick(int position) {
                ((StartActivity)getActivity()).presentKits();
            }
        });
        recyclerView.setAdapter(categoryViewAdapter);

        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);

        cancelItem = menu.findItem(R.id.cancel);
        basketItem = menu.findItem(R.id.basket);
        cancelItem.setVisible(false);
        basketItem.setVisible(false);
        infoItem = menu.getItem(0);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.info) {
            ((StartActivity)getActivity()).presentInfo();
        }

        return super.onOptionsItemSelected(item);
    }
}

