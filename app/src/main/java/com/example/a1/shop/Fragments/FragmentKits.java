package com.example.a1.shop.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ListAdapter;

import com.example.a1.shop.DataService;
import com.example.a1.shop.ItemsAtapters.KitViewAdapter;
import com.example.a1.shop.Objects.Kit;
import com.example.a1.shop.R;
import com.example.a1.shop.StartActivity;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class FragmentKits extends Fragment {

    private RecyclerView gridView;
    private ArrayList<Kit> kits;
    private KitViewAdapter adapter;

    private MenuItem cancelItem;
    private MenuItem infoItem;
    private MenuItem basketItem;


    private View.OnClickListener menuListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        setMenuVisibility(true);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.fragment_kit_list, container, false);

        gridView = view.findViewById(R.id.grid_layout);

        show();

        String title = "Категория";

        if (getActivity() != null) {
            getActivity().setTitle(title);
        }


        return view;
    }

    public void show() {
        kits = DataService.getInstance().getKitsList();
        adapter = new KitViewAdapter(getContext(), kits);
        gridView.setAdapter(adapter);

        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 2);
        gridView.setLayoutManager(layoutManager);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);

        infoItem = menu.findItem(R.id.info);
        cancelItem = menu.findItem(R.id.cancel);
        infoItem.setVisible(false);
        cancelItem.setVisible(false);
        basketItem = menu.getItem(1);

        super.onCreateOptionsMenu(menu, inflater);
    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//
//        if (item.getItemId() == R.id.cancel) {
//            ((StartActivity)getActivity()).onBackPressed();
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
}
