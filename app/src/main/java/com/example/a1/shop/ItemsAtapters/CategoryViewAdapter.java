package com.example.a1.shop.ItemsAtapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.a1.shop.DataService;
import com.example.a1.shop.Objects.Category;
import com.example.a1.shop.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;


public class CategoryViewAdapter extends RecyclerView.Adapter<CategoryViewAdapter.ViewHolder> {

    Context context;
    private List<Category> categories;
    CategoryViewAdapter.Listener selectListener;

    public CategoryViewAdapter(Context context,List<Category> categories) {
//        categories = DataService.getInstance().getCategoriesList();
        this.categories = categories;
        this.context = context;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        ViewHolder(CardView cardView) {
            super(cardView);
            this.cardView = cardView;
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        int res = R.layout.fragment_category_card;
        CardView cv = (CardView)LayoutInflater.from(parent.getContext()).inflate(res, parent, false);

        ViewHolder vh = new ViewHolder(cv);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        CardView cardView = holder.cardView;

        configureView(cardView, position, holder);
    }

    public void setSelectListener(Listener selectListener) {
        this.selectListener = selectListener;
    }

    private void configureView(CardView cardView, int position, final ViewHolder holder) {
        Category item = categories.get(position);

        TextView categoryTitle = cardView.findViewById(R.id.category_title);
        categoryTitle.setText(item.title);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (selectListener != null) {
                    selectListener.onClick(holder.getAdapterPosition());
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.size();
    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public interface Listener {
        void onClick(int position);
    }
}
