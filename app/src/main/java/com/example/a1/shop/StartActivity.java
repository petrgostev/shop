package com.example.a1.shop;

import android.os.Bundle;

import com.example.a1.shop.Fragments.FragmentCategories;
import com.example.a1.shop.Fragments.FragmentInfo;
import com.example.a1.shop.Fragments.FragmentKits;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.view.View;
import android.widget.TextView;

public class StartActivity extends AppCompatActivity {

    Toolbar toolbar;
    TextView titleEt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        titleEt = toolbar.findViewById(R.id.toolbar_title);

        setTitle(null);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        presentCategories();
    }

    @Override
    public void setTitle(CharSequence title) {
        super.setTitle("");
        titleEt.setText(title);
    }

//    @Override
//    public void onBackPressed() {
//        FragmentManager fragmentManager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
//        Fragment primaryNavigationFragment =
//                fragmentManager.getFragments().get(fragmentManager.getFragments().size() - 1);
//
//        if (fragmentManager.getBackStackEntryCount() == 0) {
//            return;
//        }
//
//        FragmentCategories fragmentCategories = new FragmentCategories();
//        fragmentTransaction.replace(R.id.frame, fragmentCategories, null);
//        fragmentTransaction.commit();
//
//        super.onBackPressed();
//    }


    public void presentCategories() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        FragmentCategories fragment = new FragmentCategories();
        fragmentTransaction.add(R.id.frame, fragment, null);
        fragmentTransaction.commit();
    }

    public void presentKits() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        FragmentKits fragment = new FragmentKits();
        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void presentInfo() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        FragmentInfo fragment = new FragmentInfo();
        fragmentTransaction.replace(R.id.frame, fragment, null);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
