package com.example.a1.shop;

import android.media.Image;

import com.example.a1.shop.Objects.Category;
import com.example.a1.shop.Objects.Kit;

import java.util.ArrayList;


public class DataService {

    private static DataService ourInstance;

    private ArrayList<Category> categories;
    private ArrayList<Kit> kits;
    private ArrayList<String> phones;

    public static DataService getInstance() {
        if (ourInstance == null) {
            ourInstance = new DataService();
        }
        return ourInstance;
    }

    public ArrayList<Category> getCategoriesList() {
        categories = new ArrayList<>();

        categories.add(new Category("Рубашка"));
        categories.add(new Category("Брюки"));
        categories.add(new Category("Пальто"));
        categories.add(new Category("Шляпа"));
        categories.add(new Category("Сапоги"));
        categories.add(new Category("Носки"));
        categories.add(new Category("Шарф"));

        return categories;
    }

    public ArrayList<Kit> getKitsList() {
        kits = new ArrayList<>();

        kits.add(new Kit("Комплект 1\n 1 500 руб"));
        kits.add(new Kit("Комплект 2\n 8 500 руб"));
        kits.add(new Kit("Комплект 3\n 3 500 руб"));
        kits.add(new Kit("Комплект 4\n 5 500 руб"));
        kits.add(new Kit("Комплект 5\n 7 500 руб"));
        kits.add(new Kit("Комплект 6\n 4 500 руб"));
        kits.add(new Kit("Комплект 7\n 2 500 руб"));
        kits.add(new Kit("Комплект 8\n 14 500 руб"));

        return kits;
    }

    public ArrayList<String> getPhonesList() {
        phones = new ArrayList<>();

        phones.add("8 902 456 46 56");
        phones.add("8 902 423 24 11");
        phones.add("8 902 234 76 54");

        return phones;
    }

    private DataService() {
        categories = new ArrayList<>();
        kits = new ArrayList<>();
        phones = new ArrayList<>();
    }
}
