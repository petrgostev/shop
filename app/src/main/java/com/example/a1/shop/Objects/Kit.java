package com.example.a1.shop.Objects;

import android.media.Image;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.util.ArrayList;

import androidx.annotation.NonNull;


public class Kit extends  BaseObject{

    static private String KIT_OWNER = "KIT_OWNER";

    public String title = "";

    public Image image;

    protected String id = "";

    private String ownerId = KIT_OWNER;

    public transient Category owner;

    public Kit(String title) {
        this.title = title;
        this.image = image;
    }

    public boolean isValid() {
        return title.length() > 0;
    }

    public int compareTo(@NonNull Object o) {
        Kit entry = (Kit) o;

        return title.compareTo(entry.title);
    }

    public String getId() {
        return id;
    }

    public String getOwnerId() {
        if (owner != null) {
            return owner.getId();
        }

        return ownerId;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String json() {
        GsonBuilder builder = new GsonBuilder().setDateFormat(DateFormat.LONG);
        Gson gson = builder.create();

        return gson.toJson(this);
    }

    public ArrayList<String> getIds() {

        ArrayList<String> ids;
        if (owner != null) {
            ids = owner.getIds();
        } else {
            ids = new ArrayList<>();
        }

        ids.add(0, id);

        return ids;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }


    public Kit clone() throws CloneNotSupportedException {
        return (Kit) super.clone();
    }

    public int compareTo(@NonNull Kit o) {
        return 0;
    }
}
