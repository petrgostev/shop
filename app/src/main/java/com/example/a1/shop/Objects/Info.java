package com.example.a1.shop.Objects;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;


public class Info extends BaseObject {

    static private String INFO_OWNER = "INFO_OWNER";

    public String description = "";

    public String conditions = "";

    public ArrayList<String> phone = new ArrayList<>();

}
