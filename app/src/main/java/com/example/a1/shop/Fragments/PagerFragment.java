package com.example.a1.shop.Fragments;

import android.media.Image;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.example.a1.shop.R;

import androidx.fragment.app.Fragment;

public class PagerFragment extends Fragment {

    public static final String ARG_IMG = "item_img";

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        // The last two arguments ensure LayoutParams are inflated
        // properly.
        View rootView = inflater.inflate(
                R.layout.pager_item, container, false);
        Bundle args = getArguments();

        ((ImageView) rootView.findViewById(R.id.item_pager)).setImageIcon(
                args.get(ARG_IMG));


        return rootView;
    }
}
