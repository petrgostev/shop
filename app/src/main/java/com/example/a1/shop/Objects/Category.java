package com.example.a1.shop.Objects;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.SerializedName;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;


public class Category extends BaseObject {

    static private String CATEGORY_OWNER = "CATEGORY_OWNER";

    public String title = "";

    protected String id = "";

    private String ownerId = CATEGORY_OWNER;

    public transient Category owner;

    public Category(String title) {
        this.title = title;
    }

    public boolean isValid() {
        return title.length() > 0;
    }

    public int compareTo(@NonNull Object o) {
        Category entry = (Category) o;

        return title.compareTo(entry.title);
    }

    public String getId() {
        return id;
    }

    public String getOwnerId() {
        if (owner != null) {
            return owner.getId();
        }

        return ownerId;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String json() {
        GsonBuilder builder = new GsonBuilder().setDateFormat(DateFormat.LONG);
        Gson gson = builder.create();

        return gson.toJson(this);
    }

    public ArrayList<String> getIds() {

        ArrayList<String> ids;
        if (owner != null) {
            ids = owner.getIds();
        } else {
            ids = new ArrayList<>();
        }

        ids.add(0, id);

        return ids;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId;
    }


    public Category clone() throws CloneNotSupportedException {
        return (Category) super.clone();
    }

    public int compareTo(@NonNull Category o) {
        return 0;
    }
}
