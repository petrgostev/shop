package com.example.a1.shop.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.a1.shop.DataService;
import com.example.a1.shop.R;
import com.example.a1.shop.StartActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;


public class FragmentInfo extends Fragment {

    protected ListView listView;
    private TextView conditionsBtn;
    private TextView conditions;
    private TextView descriptionBtn;
    private TextView description;
    private ImageView descriptionStrelka;
    private ImageView descriptionStrelka2;
    private ImageView conditionsStrelka;
    private ImageView conditionsStrelka2;


    private MenuItem cancelItem;
    private MenuItem infoItem;
    private MenuItem basketItem;

    DataService dataService;

    private View.OnClickListener menuListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
        setMenuVisibility(true);

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.fragment_info, container, false);

        listView = view.findViewById(R.id.phone_lv);

        description = view.findViewById(R.id.description_tv);
        descriptionStrelka = view.findViewById(R.id.description_strelka);
        descriptionStrelka2 = view.findViewById(R.id.description_strelka_2);
        descriptionBtn = view.findViewById(R.id.description_btn);

        conditions = view.findViewById(R.id.conditions_tv);
        conditionsStrelka = view.findViewById(R.id.conditions_strelka);
        conditionsStrelka2 = view.findViewById(R.id.conditions_strelka_2);
        conditionsBtn = view.findViewById(R.id.conditions_btn);

        descriptionStrelka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                descriptionStrelka.setVisibility(View.GONE);
                descriptionStrelka2.setVisibility(View.VISIBLE);
                description.setVisibility(View.VISIBLE);
            }
        });

        descriptionStrelka2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                descriptionStrelka2.setVisibility(View.GONE);
                descriptionStrelka.setVisibility(View.VISIBLE);
                description.setVisibility(View.GONE);
            }
        });

        conditionsStrelka.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                conditionsStrelka.setVisibility(View.GONE);
                conditionsStrelka2.setVisibility(View.VISIBLE);
                conditions.setVisibility(View.VISIBLE);
            }
        });

        conditionsStrelka2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                conditionsStrelka2.setVisibility(View.GONE);
                conditionsStrelka.setVisibility(View.VISIBLE);
                conditions.setVisibility(View.GONE);
            }
        });

        showPhones();

        String title = "Информация";

        if (getActivity() != null) {
            getActivity().setTitle(title);
        }

        return view;
    }

    public void showPhones() {
        final ArrayList<String> phones = DataService.getInstance().getPhonesList();

        ListAdapter adapter = new ArrayAdapter<String>(getContext(),android.R.layout.simple_list_item_1,phones);

        listView.setAdapter(adapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_main, menu);

        infoItem = menu.findItem(R.id.info);
        basketItem = menu.findItem(R.id.basket);
        infoItem.setVisible(false);
        basketItem.setVisible(false);
        cancelItem = menu.getItem(1);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.cancel) {
            ((StartActivity)getActivity()).onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }
}
